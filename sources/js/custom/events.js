// Event listenr

$(function(){

	var timer = setInterval(function(){
		if (window.theGame === undefined) return false;
		clearInterval(timer);
		setTimeout(function(){eventLoader()}, 0);
	}, 50);

	function eventLoader() {


		var $b = $('body');
		$b

			.on('click', function (e) {

				//closest('.keep__item', 'is-open');
				closest('.js-dd-wrap', 'is-open');
				closest('.enter__form-footnote', 'is-open');
				closest('.enter__system-wrap', 'is-open');
				closest('.sselect-wrap', 'is-open');
				closest('.system__orbit-item .planet', 'is-open');


				if ( $('.aside').hasClass('aside_open') ){
					if ( !$(e.target).closest('.aside_open').size() && !$(e.target).closest('.js-dd-asidemenu').size() ) {
						$('.aside').removeClass('aside_open');
					}
				}



				function closest(el, cls){
					if ( $(el + '.' + cls).size() ){
						if ( !$(e.target).closest(el + '.' + cls).size() ) $(el + '.' + cls).removeClass(cls);
					}
				}

			})

			.on('click', '.aside__menu-link', function (e) {
				var $t = $(this),
					$wrap = $t.closest('.aside__menu-item'),
					$submenu = $t.siblings('.aside__submenu');

				if ( $submenu.size() && !$submenu.hasClass('loading')){
					$submenu.addClass('loading');
					$wrap.toggleClass('is-open');
					$submenu.slideToggle(300, function(){
						$submenu.removeClass('loading');
					});
				}

			})

			.on('click', '.js-dd-asidemenu', function (e) {
				$('.aside').toggleClass('aside_open');
			})

			.on('click', '.js-dd-link', function (e) {
				var $wrap = $(this).closest('.js-dd-wrap');
				$('.js-dd-wrap').not($wrap).removeClass('is-open');
				$wrap.toggleClass('is-open');
			})
			.on('click', '.is-compact', function (e) {
				$(this).removeClass('is-compact');
			})
			.on('click', '.game__force-close', function (e) {
				$(this).closest('.game__force-group').addClass('is-compact');
			})
			.on('click', '.game__menu-close', function (e) {
				$(this).closest('.game__menu-item').addClass('is-compact');
			})

			/*.on('click', '.keep__item-preview', function (e) {
				var $wrap = $(this).closest('.keep__item');

				$('.keep__item.is-open').not($wrap).removeClass('is-open');

				if ($wrap.find('.keep__item-info').size() ) $wrap.addClass('is-open');
			})*/
			.on('click', '.keep__item-info-close', function (e) {
				$(this).closest('.keep__item').removeClass('is-open');
			})






			.on('click', '.game__login-form', function (e) {
				var $form = $(this);

				if ( $form.hasClass('is-closed') ){
					$form
						.addClass('is-open')
						.find('.enter__form')
						.slideDown(300, function(){
							$(this).closest('.game__login-form').removeClass('is-closed');
						});

					$form
						.siblings('.is-open')
						.removeClass('is-open')
						.find('.enter__form')
						.slideUp(300, function(){
							$(this).closest('.game__login-form').addClass('is-closed');
						});
				}
			})

			.on('click', '.enter__close', function (e) {
				var $form = $(this).closest('.game__login-form');

				if ( $form.hasClass('is-open') ){
					$form
						.removeClass('is-open')
						.find('.enter__form')
						.slideUp(300, function(){
							$(this).closest('.game__login-form').addClass('is-closed');
						})
						.find('.enter__form-field').removeClass('is-error');
				}
			})

			.on('mousedown', '.enter__form-controls-clear', function (e) {
				var $input = $(this).closest('.enter__form-input').find('input');

				$input.val('');
				setTimeout(function(){
					$input.focus()
				}, 0);
			})





			.on('click', '.enter__form-footnote-link', function (e) {
				var $t = $(this),
					$wrap = $t.closest('.enter__form-footnote'),
					$tooltip = $wrap.find('.enter__form-footnote-tooltip');

				if ( $tooltip.size() ){
					e.preventDefault();
					$wrap.toggleClass('is-open');
				}
			})




			.on('click', '.enter__system-current, .enter__system-handle', function (e) {
				var $t = $(this),
					$wrap = $t.closest('.enter__system-wrap');

				$wrap.toggleClass('is-open');

			})

			.on('click', '.enter__system-item', function (e) {
				var $t = $(this),
					$wrap = $t.closest('.enter__system-wrap'),
					$current = $wrap.find('.enter__system-current'),
					$input = $wrap.find('input');


				$current.text($t.text());
				$input.val($t.data('value'));
				$wrap.removeClass('is-open');

			})


			.on('click', '.sselect-current, .sselect-handle', function (e) {
				var $t = $(this),
					$wrap = $t.closest('.sselect-wrap');

				$wrap.toggleClass('is-open');

			})

			.on('click', '.sselect-item', function (e) {
				var $t = $(this),
					$wrap = $t.closest('.sselect-wrap'),
					$current = $wrap.find('.sselect-current'),
					$input = $wrap.find('input');


				$current.text($t.text());
				$input.val($t.data('value'));
				$wrap.removeClass('is-open');

			})



			.on('click', '.js-submit-form', function(e){
				e.preventDefault();
				$(this).closest('form').submit();
			})
			.on('submit', '.game__login-form', function(e){
				e.preventDefault();
				var $form = $(this);


				$form
					.find('.enter__form-field')
					.removeClass('is-error is-success')
					.find('.enter__form-message')
					.html('');

				if (theGame.checkForm($form)) {

					$.ajax({
						type: $form.attr('method'),
						url: $form.attr('action'),
						data: $form.serialize(),
						complete: function (res) {

							var data = JSON.parse(res.responseText);


							if (data.errors.length){

								var i = 0;

								for (i; i<data.errors.length; i++){

									var $input = $form.find('[name="' + data.errors[i].name + '"]'),
										$field = $input.closest('.enter__form-field');

									$field
										.addClass('is-error')
										.find('.enter__form-message')
										.html(data.errors[i].message);
								}

							}


						}
					});
				}
			})




			// popups

			.on('click', '.planet__item-name-title', function(e){
				var $t = $(this);

				$t
					.closest('.planet__item')
					.find('.planet__item-all')
					.slideToggle(200, function(){
						$t.closest('.planet__item').toggleClass('is-open');
					})
				;
			})

			.on('click', '.force__item-name-title', function(e){
				var $t = $(this);

				$t
					.closest('.force__item')
					.find('.force__item-all')
					.slideToggle(200, function(){
						$t.closest('.force__item').toggleClass('is-open');
					})
				;
			})

			.on('click', '.popup__pp-close', function(e){
				$(this).closest('.popup__pp').removeClass('is-open');
			})


			.on('click', '.build__item', function(e){
				var $t = $(this);

				if ( !$t.hasClass('is-locked')){

					$t.closest('.popup').find('.popup__pp').addClass('is-open');

				}
			})


			.on('click', '.fleet__item-inner', function(e){
				var $t = $(this);

				if ( !$t.hasClass('is-locked')){

					$t.closest('.popup').find('.popup__pp').addClass('is-open');

				}
			})


			.on('click', '.system__orbit-item .planet__item', function(e){
				var $t = $(this),
					$p = $t.closest('.planet');

				$('.system__orbit-item .planet').not($p).removeClass('is-open');

				if ( !$p.hasClass('is-locked') && $p.find('.planet__info').size() ){
					$p.addClass('is-open');

				}
			})


		;

	}

});
